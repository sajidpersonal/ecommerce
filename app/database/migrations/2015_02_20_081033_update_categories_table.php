<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCategoriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            DB::statement('ALTER TABLE `categories` CHANGE `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT;');
            DB::statement('ALTER TABLE `products` CHANGE `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT;');
            DB::statement('ALTER TABLE `theme` CHANGE `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT;');
            DB::statement('ALTER TABLE `mail` CHANGE `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT;');
            DB::statement('ALTER TABLE `error_logs` CHANGE `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT;');
            
            Schema::table('categories', function($table){$table->timestamps();});
            Schema::table('products', function($table){$table->timestamps();});
            Schema::table('theme', function($table){$table->timestamps();});
            Schema::table('mail', function($table){$table->timestamps();});
            
            Schema::table('error_logs', function($table){$table->dropColumn('created_at');});
            Schema::table('error_logs', function($table){$table->timestamps();});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            DB::statement('ALTER TABLE `categories` CHANGE `id` `id` BIGINT(20) NOT NULL;');
            DB::statement('ALTER TABLE `products` CHANGE `id` `id` BIGINT(20) NOT NULL;');
            DB::statement('ALTER TABLE `theme` CHANGE `id` `id` BIGINT(20) NOT NULL;');
            DB::statement('ALTER TABLE `mail` CHANGE `id` `id` BIGINT(20) NOT NULL;');
            DB::statement('ALTER TABLE `error_logs` CHANGE `id` `id` BIGINT(20) NOT NULL;');
            
            Schema::table('categories', function($table){$table->dropTimestamps();});
            Schema::table('products', function($table){$table->dropTimestamps();});
            Schema::table('theme', function($table){$table->dropTimestamps();});
            Schema::table('mail', function($table){$table->dropTimestamps();});
            
            Schema::table('error_logs', function($table){$table->dropTimestamps();});
            Schema::table('error_logs', function($table){$table->timestamp('created_at');});
	}

}
