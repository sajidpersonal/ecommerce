<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::table('mail', function($table){
                $table->bigInteger('to');
                $table->string('subject', 500);
                $table->text('message');
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            Schema::table('mail', function($table){
                $table->dropColumn('to');
                $table->dropColumn('subject');
                $table->dropColumn('message');
            });
	}

}
