<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
            Schema::drop('customers');
            DB::statement('ALTER TABLE `users` CHANGE `id` `id` BIGINT(20) NOT NULL AUTO_INCREMENT;');
            Schema::table('users', function($table)
            {
                //$table->bigIncrements('id');
                $table->string('fname', 500);
                $table->string('lname', 500);
                $table->string('class', 500);
                $table->string('room', 500);
                $table->string('building', 500);
                $table->string('address1', 500);
                $table->string('address2', 500);
                $table->string('city', 500);
                $table->string('state', 500);
                $table->string('postal_code', 500);
                $table->string('country', 500);
                $table->string('phone', 500);
                $table->string('voice_mail', 500);
                $table->string('billing_address', 500);
                $table->string('billing_city', 500);
                $table->string('billing_region', 500);
                $table->string('billing_postal_code', 500);
                $table->string('billing_country', 500);
                $table->string('ship_address', 500);
                $table->string('ship_city', 500);
                $table->string('ship_region', 500);
                $table->string('ship_postalcode', 500);
                $table->string('ship_country', 500);
            });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
            Schema::create('customers', function($table)
            {
                $table->bigInteger('id');
                $table->primary('id');
                $table->string('fname', 500);
                $table->string('lname', 500);
                $table->string('class', 500);
                $table->string('room', 500);
                $table->string('building', 500);
                $table->string('address1', 500);
                $table->string('address2', 500);
                $table->string('city', 500);
                $table->string('state', 500);
                $table->string('postal_code', 500);
                $table->string('country', 500);
                $table->string('phone', 500);
                $table->string('email', 500);
                $table->string('voice_mail', 500);
                $table->string('billing_address', 500);
                $table->string('billing_city', 500);
                $table->string('billing_region', 500);
                $table->string('billing_postal_code', 500);
                $table->string('billing_country', 500);
                $table->string('ship_address', 500);
                $table->string('ship_city', 500);
                $table->string('ship_region', 500);
                $table->string('ship_postalcode', 500);
                $table->string('ship_country', 500);
                $table->timestamp('date_entered');
            });
            DB::statement('ALTER TABLE `users` CHANGE `id` `id` BIGINT(20) NOT NULL;');
            Schema::table('users', function($table)
            {
                $table->dropColumn('fname');
                $table->dropColumn('lname');
                $table->dropColumn('class');
                $table->dropColumn('room');
                $table->dropColumn('building');
                $table->dropColumn('address1');
                $table->dropColumn('address2');
                $table->dropColumn('city');
                $table->dropColumn('state');
                $table->dropColumn('postal_code');
                $table->dropColumn('country');
                $table->dropColumn('phone');
                $table->dropColumn('voice_mail');
                $table->dropColumn('billing_address');
                $table->dropColumn('billing_city');
                $table->dropColumn('billing_region');
                $table->dropColumn('billing_postal_code');
                $table->dropColumn('billing_country');
                $table->dropColumn('ship_address');
                $table->dropColumn('ship_city');
                $table->dropColumn('ship_region');
                $table->dropColumn('ship_postalcode');
                $table->dropColumn('ship_country');
            });
	}

}
