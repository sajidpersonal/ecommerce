<?php

//use Cribbb\Storage\User\UserRepository as User;

class AdminController extends BaseController {

    public function __construct(){
        if(!Parent::isAdmin()){Redirect::to('login');}
    }

    /**
     * User Repository
     */
    protected $user;

    /**
     * Inject the User Repository
     */
    public function __construct(User $user) {
        $this->user = $user;
    }

    public function index() {
        return View::make('admin.dashboard');
    }
}