<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
        function isAdmin(){
            if(Auth::check()){
                if(Auth::user()->role == 10){
                    return true;
                }
            }
            return false;
        }

}
