<?php

class AdminErrorsLogController extends \BaseController {

        public function __construct(){
            if(!Parent::isAdmin()){Redirect::to('login');}
        }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//

            $el = ErrorsLog::paginate(1);

            // load the view and pass the product
            return View::make('admin.errors_log.listing')
            ->with('el', $el);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
            $el = ErrorsLog::find($id);
            $el->delete();
            
            Session::flash('message', 'Error Deleted Successfully!');
            return Redirect::to('admin/errors_log');
	}


}
