<?php

class AdminProductController extends \BaseController {

        public function __construct(){
            if(!Parent::isAdmin()){Redirect::to('login');}
        }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
            //$product = Product::all();

            $product = Product::paginate(1);

            //$product = Product::where('votes', '>', 100)->paginate(15);
            
            // load the view and pass the product
            return View::make('admin.product.listing')
            ->with('product', $product);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            $categories = Category::getAllCategories();
            array_unshift($categories, "Select Parent Category");
            return View::make('admin.product.create', array('categories'=>$categories));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            // validate
            // read more on validation at http://laravel.com/docs/validation
            $rules = array(
                'pname'       => 'required'
            );
            $validator = Validator::make(Input::all(), $rules);

            // process the login
            if ($validator->fails()) {
                return Redirect::to('admin/product/create')
                    ->withErrors($validator)
                    ->withInput(Input::except('password'));
            } else {
                // store
                $product = new Product;
                $product->pname       = Input::get('pname');
                $product->pdescription      = Input::get('pdescription');
                $product->category_id   = Input::get('category_id');

                $product->sku       = Input::get('sku');
                $product->quantity_per_unit       = Input::get('quantity_per_unit');
                $product->unit_price       = Input::get('unit_price');
                $product->available_size       = Input::get('available_size');
                $product->available_colors       = Input::get('available_colors');
                $product->size       = Input::get('size');
                $product->color       = Input::get('color');
                $product->discount       = Input::get('discount');
                $product->unit_weight       = Input::get('unit_weight');
                $product->note       = Input::get('note');

                $product->save();

                // redirect
                Session::flash('message', 'Product Created Successfully!');
                return Redirect::to('admin/product');
            }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
            $product = Product::find($id);
            $product['category'] = Category::getName($product->category_id);
            
            return View::make('admin.product.show')->with('product', $product);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
            $product = Product::find($id);
            $categories = Category::getAllCategories();
            array_unshift($categories, "Select Parent Category");
            return View::make('admin.product.edit', array('product'=>$product, 'categories'=>$categories));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
            $rules = array(
                'pname' => 'required'
            );
            $validator = Validator::make(Input::all(), $rules);
            if($validator->fails()){
                return Redirect::to('admin/product/'.$id.'/edit')->withErrors($validator)->withInput();
            }else{
                $product = Product::find($id);
                $product->pname = Input::get('pname');
                $product->pdescription = Input::get('pdescription');
                $product->category_id = Input::get('category_id');
                
                $product->sku       = Input::get('sku');
                $product->quantity_per_unit       = Input::get('quantity_per_unit');
                $product->unit_price       = Input::get('unit_price');
                $product->available_size       = Input::get('available_size');
                $product->available_colors       = Input::get('available_colors');
                $product->size       = Input::get('size');
                $product->color       = Input::get('color');
                $product->discount       = Input::get('discount');
                $product->unit_weight       = Input::get('unit_weight');
                $product->note       = Input::get('note');
                
                $product->save();
                
                Session::flash('message', 'Product Updated Successfully!');
                return Redirect::to('admin/product');
            }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
            $product = Product::find($id);
            $product->delete();
            
            Session::flash('message', 'Product Deleted Successfully!');
            return Redirect::to('admin/product');
	}


}
