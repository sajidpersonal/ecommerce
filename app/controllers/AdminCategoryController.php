<?php

class AdminCategoryController extends \BaseController {

        public function __construct(){
            if(!Parent::isAdmin()){Redirect::to('login');}
        }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
            //$category = Category::all();

            $category = Category::paginate(1);

            //$category = Category::where('votes', '>', 100)->paginate(15);
            
            // load the view and pass the category
            return View::make('admin.category.listing')
            ->with('category', $category);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            
            $all_categories = Category::getAllCategories();
            array_unshift($all_categories, "Select Parent Category");
            return View::make('admin.category.create', array('all_categories'=>$all_categories));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            // validate
            // read more on validation at http://laravel.com/docs/validation
            $rules = array(
                'cname'       => 'required'
            );
            $validator = Validator::make(Input::all(), $rules);

            // process the login
            if ($validator->fails()) {
                return Redirect::to('admin/category/create')
                    ->withErrors($validator)
                    ->withInput(Input::except('password'));
            } else {
                // store
                $category = new Category;
                $category->cname       = Input::get('cname');
                $category->description      = Input::get('description');
                $category->parent_id      = Input::get('parent_id');
                $category->save();

                // redirect
                Session::flash('message', 'Category Created Successfully!');
                return Redirect::to('admin/category');
            }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
            $category = Category::find($id);
            $category['parent_category'] = Category::getName($category->parent_id);
            return View::make('admin.category.show')->with('category', $category);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
            $all_categories = Category::getAllCategories();
            array_unshift($all_categories, "Select Parent Category");
            $category = Category::find($id);
            return View::make('admin.category.edit', array('category'=>$category, 'all_categories'=>$all_categories));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
            $rules = array(
                'cname' => 'required'
            );
            $validator = Validator::make(Input::all(), $rules);
            if($validator->fails()){
                return Redirect::to('admin/category/'.$id.'/edit')->withErrors($validator)->withInput(Input::except('password'));
            }else{
                $category = Category::find($id);
                $category->cname = Input::get('cname');
                $category->description = Input::get('description');
                $category->parent_id = Input::get('parent_id');
                
                $category->save();
                
                Session::flash('message', 'Category Updated Successfully!');
                return Redirect::to('admin/category');
            }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
            $category = Category::find($id);
            $category->delete();
            
            Session::flash('message', 'Category Deleted Successfully!');
            return Redirect::to('admin/category');
	}
        
}
