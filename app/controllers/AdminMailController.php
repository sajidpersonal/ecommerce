<?php

class AdminMailController extends \BaseController {

        public function __construct(){
            if(!Parent::isAdmin()){Redirect::to('login');}
        }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
            //$product = Product::all();

            $product = Product::paginate(1);

            //$product = Product::where('votes', '>', 100)->paginate(15);
            
            // load the view and pass the product
            return View::make('admin.product.listing')
            ->with('product', $product);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function compose()
	{
            $users = User::getAllUsers();
            array_unshift($users, "Select User");
            return View::make('admin.mail.compose', array('users'=>$users));
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function send()
	{
            // validate
            // read more on validation at http://laravel.com/docs/validation
            $rules = array(
                'to'            => 'required|numeric|min:1',
                'subject'       => 'required',
                'message'       => 'required'
            );
            $validator = Validator::make(Input::all(), $rules);

            // process the login
            if ($validator->fails()) {
                return Redirect::to('admin/mail')
                    ->withErrors($validator)
                    ->withInput();
            } else {
                // store
                $email = new EMail;
                $user = User::find(Input::get('to'));
                $email->to            = Input::get('to');
                $email->mail_address  = $user->email;
                $email->subject       = Input::get('subject');
                $email->message       = Input::get('message');

                $email->save();
                
                /*Mail::send($email->message, array('email'=>$user->email), function($message) use ($email)
                {
                    $message->to($email->mail_address, $email->mail_address)->from("sajid@nxvt.com", "Sajid")->subject(Input::get('subject'));
                });*/

                // redirect
                Session::flash('message', 'Email Sent Successfully!');
                return Redirect::to('admin/mail');
            }
	}
}
