<?php

class AdminUsersController extends \BaseController {
    
        public function __construct(){
            if(!Parent::isAdmin()){Redirect::to('login');}
        }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
            
            $users = User::paginate(1);

            //$users = User::where('votes', '>', 100)->paginate(15);
            
            // load the view and pass the users
            return View::make('admin.users.listing')
            ->with('users', $users);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
            return View::make('admin.users.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            // validate
            // read more on validation at http://laravel.com/docs/validation
            $rules = array(
                'username'       => 'required|unique:users',
                'email'      => 'required|email|unique:users',
                'password'      => 'required|min:8',
            );
            $validator = Validator::make(Input::all(), $rules);

            // process the login
            if ($validator->fails()) {
                return Redirect::to('admin/users/create')
                    ->withErrors($validator)
                    ->withInput(Input::except('password'));
            } else {
                // store
                $user = new User;
                $user->username       = Input::get('username');
                $user->email      = Input::get('email');
                $user->password   = Hash::make(Input::get('password'));
                $user->fname      = Input::get('fname');
                $user->lname      = Input::get('lname');
                $user->address1      = Input::get('address1');
                $user->address2      = Input::get('address2');
                $user->city      = Input::get('city');
                $user->state      = Input::get('state');
                $user->postal_code      = Input::get('postal_code');
                $user->country      = Input::get('country');
                $user->phone      = Input::get('phone');
                $user->billing_address      = Input::get('billing_address');
                $user->billing_city      = Input::get('billing_city');
                $user->billing_region      = Input::get('billing_region');
                $user->billing_postal_code      = Input::get('billing_postal_code');
                $user->billing_country      = Input::get('billing_country');
                $user->ship_address      = Input::get('ship_address');
                $user->ship_city      = Input::get('ship_city');
                $user->ship_region      = Input::get('ship_region');
                $user->ship_postalcode      = Input::get('ship_postalcode');
                $user->ship_country      = Input::get('ship_country');
                $user->save();

                // redirect
                Session::flash('message', 'User Created Successfully!');
                return Redirect::to('admin/users');
            }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
            $user = User::find($id);
            
            return View::make('admin.users.show')->with('user', $user);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
            $user = User::find($id);
            return View::make('admin.users.edit')->with('user', $user);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
            $rules = array(
                'password' => 'min:8'
            );
            $validator = Validator::make(Input::all(), $rules);
            if($validator->fails()){
                return Redirect::to('admin/users/'.$id.'/edit')->withErrors($validator)->withInput(Input::except('password'));
            }else{
                $user = User::find($id);
                $user->password = Hash::make(Input::get('password'));
                $user->fname = Input::get('fname');
                $user->lname = Input::get('lname');
                
                $user->address1 = Input::get('address1');
                $user->address2 = Input::get('address2');
                $user->city = Input::get('city');
                $user->state = Input::get('state');
                $user->postal_code = Input::get('postal_code');
                $user->country = Input::get('country');
                $user->phone = Input::get('phone');
                $user->voice_mail = Input::get('voice_mail');
                $user->billing_address = Input::get('billing_address');
                $user->billing_city = Input::get('billing_city');
                $user->billing_region = Input::get('billing_region');
                $user->billing_postal_code = Input::get('billing_postal_code');
                $user->billing_country = Input::get('billing_country');
                $user->ship_address = Input::get('ship_address');
                $user->ship_city = Input::get('ship_city');
                $user->ship_region = Input::get('ship_region');
                $user->ship_postalcode = Input::get('ship_postalcode');
                $user->ship_country = Input::get('ship_country');
                $user->save();
                
                Session::flash('message', 'User Updated Successfully!');
                return Redirect::to('admin/users');
            }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
            $user = User::find($id);
            $user->delete();
            
            Session::flash('message', 'User Deleted Successfully!');
            return Redirect::to('admin/users');
	}
        
}
