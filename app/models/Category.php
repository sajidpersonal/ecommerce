<?php

class Category extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'categories';

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */

        public static function getAllCategories(){
            $categories = Parent::get(array('id', 'cname'));
            
            if(count($categories) > 0)
            foreach($categories as $category){
                $all_categories[$category->id] = $category->cname;
            }
            return $all_categories;
        }
        
        public static function getName($id = 0){
            $cname = Category::where('id', '=', $id)->take(1)->get();
            if(count($cname) > 0)
                return $cname[0]->cname;
            return "No";
        }
}
