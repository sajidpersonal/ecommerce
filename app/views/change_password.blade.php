@extends('layout')
@section('content')
@if($errors->any())
  <ul>
    {{ implode('', $errors->all('<li>:message</li>'))}}
  </ul>
@endif
{{ Form::open(array('route' => 'password/change')) }}
 
  <p>{{ Form::label('password', 'New Password') }}
  {{ Form::password('password') }}</p>
 
  <p>{{ Form::label('password_confirmation', 'Confirm Password') }}
  {{ Form::password('password_confirmation') }}</p>
 
  <p>{{ Form::submit('Submit') }}</p>
 
{{ Form::close() }}
@stop