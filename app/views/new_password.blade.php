@extends('layout')
@section('content')
@if($errors->any())
  <ul>
    {{ implode('', $errors->all('<li>:message</li>'))}}
  </ul>
@endif
{{ Form::open(array('route' => 'password/reset')) }}{{ Form::hidden('token', $token) }}
 
  <p>{{ Form::label('password', 'New Password') }}
  {{ Form::password('password') }}</p>
 
  <p>{{ Form::label('password_confirmation', 'Confirm Password') }}
  {{ Form::password('password_confirmation') }}</p>
 
  <p>{{ Form::submit('Submit') }}</p>
 
{{ Form::close() }}
<a href="<?php echo action('login', array());?>">Login</a>
@stop