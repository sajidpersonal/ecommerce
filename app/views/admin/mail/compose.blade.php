@extends('admin.layout')
@section('content')

<h1>Compose Mail</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}
@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

{{ Form::open(array('url' => 'admin/mail')) }}

    <div class="form-group">
        {{ Form::label('to', 'To:') }}
        {{ Form::select('to', $users, Input::old('to')) }}
    </div>

    <div class="form-group">
        {{ Form::label('subject', 'Subject') }}
        {{ Form::text('subject', Input::old('subject'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('message', 'Message') }}
        {{ Form::textarea('message', Input::old('message'), array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Send!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

@stop