@extends('admin.layout')
@section('content')

<h1>Edit {{ $user->username }}</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::model($user, array('route' => array('admin.users.update', $user->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('email', 'Email') }}
        {{ $user->email }}
    </div>

    <div class="form-group">
        {{ Form::label('password', 'Password') }}
        {{ Form::password('password', '', array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('fname', 'First Name') }}
        {{ Form::text('fname', $user->fname, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('lname', 'Last Name') }}
        {{ Form::text('lname', $user->lname, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('address1', 'Address1') }}
        {{ Form::text('address1', $user->address1, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('address2', 'Address 2') }}
        {{ Form::text('address2', $user->address2, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('city', 'City') }}
        {{ Form::text('city', $user->city, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('state', 'State') }}
        {{ Form::text('state', $user->state, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('postal_code', 'Postal Code') }}
        {{ Form::text('postal_code', $user->postal_code, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('country', 'Country') }}
        {{ Form::text('country', $user->country, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('phone', 'Phone') }}
        {{ Form::text('phone', $user->phone, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('billing_address', 'Billing Address') }}
        {{ Form::text('billing_address', $user->billing_address, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('billing_city', 'Billing City') }}
        {{ Form::text('billing_city', $user->billing_city, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('billing_region', 'Billing Region') }}
        {{ Form::text('billing_region', $user->billing_region, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('billing_postal_code', 'Billing Postal Code') }}
        {{ Form::text('billing_postal_code', $user->billing_postal_code, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('billing_country', 'Billing Country') }}
        {{ Form::text('billing_country', $user->billing_country, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('ship_address', 'Ship Address') }}
        {{ Form::text('ship_address', $user->ship_address, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('ship_city', 'Ship City') }}
        {{ Form::text('ship_city', $user->ship_city, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('ship_region', 'Ship Region') }}
        {{ Form::text('ship_region', $user->ship_region, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('ship_postalcode', 'Ship Postal Code') }}
        {{ Form::text('ship_postalcode', $user->ship_postalcode, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('ship_country', 'Ship Country') }}
        {{ Form::text('ship_country', $user->ship_country, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Update the User!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

@stop