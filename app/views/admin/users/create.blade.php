@extends('admin.layout')
@section('content')

<h1>Create a User</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::open(array('url' => 'admin/users')) }}

    <div class="form-group">
        {{ Form::label('username', 'User Name') }}
        {{ Form::text('username', Input::old('username'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('email', 'Email') }}
        {{ Form::email('email', Input::old('email'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('password', 'Password') }}
        {{ Form::password('password', Input::old('password'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('fname', 'First Name') }}
        {{ Form::text('fname', Input::old('fname'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('lname', 'Last Name') }}
        {{ Form::text('lname', Input::old('lname'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('address1', 'Address1') }}
        {{ Form::text('address1', Input::old('address1'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('address2', 'Address 2') }}
        {{ Form::text('address2', Input::old('address2'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('city', 'City') }}
        {{ Form::text('city', Input::old('city'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('state', 'State') }}
        {{ Form::text('state', Input::old('state'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('postal_code', 'Postal Code') }}
        {{ Form::text('postal_code', Input::old('postal_code'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('country', 'Country') }}
        {{ Form::text('country', Input::old('country'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('phone', 'Phone') }}
        {{ Form::text('phone', Input::old('phone'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('billing_address', 'Billing Address') }}
        {{ Form::text('billing_address', Input::old('billing_address'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('billing_city', 'Billing City') }}
        {{ Form::text('billing_city', Input::old('billing_city'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('billing_region', 'Billing Region') }}
        {{ Form::text('billing_region', Input::old('billing_region'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('billing_postal_code', 'Billing Postal Code') }}
        {{ Form::text('billing_postal_code', Input::old('billing_postal_code'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('billing_country', 'Billing Country') }}
        {{ Form::text('billing_country', Input::old('billing_country'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('ship_address', 'Ship Address') }}
        {{ Form::text('ship_address', Input::old('ship_address'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('ship_city', 'Ship City') }}
        {{ Form::text('ship_city', Input::old('ship_city'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('ship_region', 'Ship Region') }}
        {{ Form::text('ship_region', Input::old('ship_region'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('ship_postalcode', 'Ship Postal Code') }}
        {{ Form::text('ship_postalcode', Input::old('ship_postalcode'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('ship_country', 'Ship Country') }}
        {{ Form::text('ship_country', Input::old('ship_country'), array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Create the User!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

@stop