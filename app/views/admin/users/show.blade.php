@extends('admin.layout')
@section('content')

<h1>{{ $user->username }}</h1>

    <div class="form-group">
        {{ Form::label('email', 'Email') }}
        {{ $user->email }}
    </div>

    <div class="form-group">
        {{ Form::label('password', 'Password') }}
        ********
    </div>

    <div class="form-group">
        {{ Form::label('fname', 'First Name') }}
        {{ $user->fname }}
    </div>

    <div class="form-group">
        {{ Form::label('lname', 'Last Name') }}
        {{ $user->lname }}
    </div>

    <div class="form-group">
        {{ Form::label('address1', 'Address1') }}
        {{ $user->address1 }}
    </div>

    <div class="form-group">
        {{ Form::label('address2', 'Address 2') }}
        {{ $user->address2 }}
    </div>

    <div class="form-group">
        {{ Form::label('city', 'City') }}
        {{ $user->city }}
    </div>

    <div class="form-group">
        {{ Form::label('state', 'State') }}
        {{ $user->state }}
    </div>

    <div class="form-group">
        {{ Form::label('postal_code', 'Postal Code') }}
        {{ $user->postal_code }}
    </div>

    <div class="form-group">
        {{ Form::label('country', 'Country') }}
        {{ $user->country }}
    </div>

    <div class="form-group">
        {{ Form::label('phone', 'Phone') }}
        {{ $user->phone }}
    </div>

    <div class="form-group">
        {{ Form::label('billing_address', 'Billing Address') }}
        {{ $user->billing_address }}
    </div>

    <div class="form-group">
        {{ Form::label('billing_city', 'Billing City') }}
        {{ $user->billing_city }}
    </div>

    <div class="form-group">
        {{ Form::label('billing_region', 'Billing Region') }}
        {{ $user->billing_region }}
    </div>

    <div class="form-group">
        {{ Form::label('billing_postal_code', 'Billing Postal Code') }}
        {{ $user->billing_postal_code }}
    </div>

    <div class="form-group">
        {{ Form::label('billing_country', 'Billing Country') }}
        {{ $user->billing_country }}
    </div>

    <div class="form-group">
        {{ Form::label('ship_address', 'Ship Address') }}
        {{ $user->ship_address }}
    </div>

    <div class="form-group">
        {{ Form::label('ship_city', 'Ship City') }}
        {{ $user->ship_city }}
    </div>

    <div class="form-group">
        {{ Form::label('ship_region', 'Ship Region') }}
        {{ $user->ship_region }}
    </div>

    <div class="form-group">
        {{ Form::label('ship_postalcode', 'Ship Postal Code') }}
        {{ $user->ship_postalcode }}
    </div>

    <div class="form-group">
        {{ Form::label('ship_country', 'Ship Country') }}
        {{ $user->ship_country }}
    </div>

@stop