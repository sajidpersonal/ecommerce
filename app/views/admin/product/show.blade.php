@extends('admin.layout')
@section('content')

<h1>{{ $product->pname }}</h1>

    <div class="form-group">
        {{ Form::label('pname', 'Name') }}
        {{ $product->pname }}
    </div>
    <div class="form-group">
        {{ Form::label('pdescription', 'Description') }}
        {{ $product->pdescription }}
    </div>
    <div class="form-group">
        {{ Form::label('category', 'Category') }}
        {{ $product->category }}
    </div>
    <div class="form-group">
        {{ Form::label('sku', 'SKU') }}
        {{ $product->sku }}
    </div>
    <div class="form-group">
        {{ Form::label('quantity_per_unit', 'Quantity Per Unit') }}
        {{ $product->quantity_per_unit }}
    </div>
    <div class="form-group">
        {{ Form::label('unit_price', 'Unit Price') }}
        {{ $product->unit_price }}
    </div>
    <div class="form-group">
        {{ Form::label('available_size', 'Available Size') }}
        {{ $product->available_size }}
    </div>
    <div class="form-group">
        {{ Form::label('available_colors', 'Available Colors') }}
        {{ $product->available_colors }}
    </div>
    <div class="form-group">
        {{ Form::label('size', 'Size') }}
        {{ $product->size }}
    </div>
    <div class="form-group">
        {{ Form::label('color', 'Color') }}
        {{ $product->color }}
    </div>
    <div class="form-group">
        {{ Form::label('discount', 'Discount') }}
        {{ $product->discount }}
    </div>
    <div class="form-group">
        {{ Form::label('unit_weight', 'Unit Weight') }}
        {{ $product->unit_weight }}
    </div>
    <div class="form-group">
        {{ Form::label('note', 'Notes') }}
        {{ $product->note }}
    </div>

@stop