@extends('admin.layout')
@section('content')

<h1>Edit {{ $product->pname }}</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::model($product, array('route' => array('admin.product.update', $product->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('pname', 'Name') }}
        {{ Form::text('pname', $product->pname, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('pdescription', 'Description') }}
        {{ Form::textarea('pdescription', $product->pdescription, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('category_id', 'Category') }}
        {{ Form::select('category_id', $categories, $product->category_id) }}
    </div>
    <div class="form-group">
        {{ Form::label('sku', 'SKU') }}
        {{ Form::text('sku', $product->sku, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('quantity_per_unit', 'Quantity Per Unit') }}
        {{ Form::text('quantity_per_unit', $product->quantity_per_unit, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('unit_price', 'Unit Price') }}
        {{ Form::text('unit_price', $product->unit_price, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('available_size', 'Available Size') }}
        {{ Form::text('available_size', $product->available_size, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('available_colors', 'Available Colors') }}
        {{ Form::text('available_colors', $product->available_colors, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('size', 'Size') }}
        {{ Form::text('size', $product->size, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('color', 'Color') }}
        {{ Form::text('color', $product->color, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('discount', 'Discount') }}
        {{ Form::text('discount', $product->discount, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('unit_weight', 'Unit Weight') }}
        {{ Form::text('unit_weight', $product->unit_weight, array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('note', 'Notes') }}
        {{ Form::textarea('note', $product->note, array('class' => 'form-control')) }}
    </div>

{{ Form::submit('Update the Product!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

@stop