@extends('admin.layout')
@section('content')

@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Actions</td>
        </tr>
    </thead>
    <tbody>
    @foreach($product as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->pname }}</td>

            <!-- we will also add show, edit, and delete buttons -->
            <td>

                <!-- delete the user (uses the destroy method DESTROY /product/{id} -->
                {{ Form::open(array('url' => 'admin/product/'.$value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete this Product', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}

                <!-- show the user (uses the show method found at GET /product/{id} -->
                <a class="btn btn-small btn-success" href="{{ URL::to('admin/product/' . $value->id) }}">Show this User</a>

                <!-- edit this user (uses the edit method found at GET /product/{id}/edit -->
                <a class="btn btn-small btn-info" href="{{ URL::to('admin/product/' . $value->id . '/edit') }}">Edit this User</a>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<?php echo $product->links(); ?>
@stop