@extends('admin.layout')
@section('content')

<h1>Create a Product</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::open(array('url' => 'admin/product')) }}

    <div class="form-group">
        {{ Form::label('pname', 'Name') }}
        {{ Form::text('pname', Input::old('pname'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('pdescription', 'Description') }}
        {{ Form::textarea('pdescription', Input::old('pdescription'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('category_id', 'Category') }}
        {{ Form::select('category_id', $categories, Input::old('category_id')) }}
    </div>
    <div class="form-group">
        {{ Form::label('sku', 'SKU') }}
        {{ Form::text('sku', Input::old('sku'), array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('quantity_per_unit', 'Quantity Per Unit') }}
        {{ Form::text('quantity_per_unit', Input::old('quantity_per_unit'), array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('unit_price', 'Unit Price') }}
        {{ Form::text('unit_price', Input::old('unit_price'), array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('available_size', 'Available Size') }}
        {{ Form::text('available_size', Input::old('available_size'), array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('available_colors', 'Available Colors') }}
        {{ Form::text('available_colors', Input::old('available_colors'), array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('size', 'Size') }}
        {{ Form::text('size', Input::old('size'), array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('color', 'Color') }}
        {{ Form::text('color', Input::old('color'), array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('discount', 'Discount') }}
        {{ Form::text('discount', Input::old('discount'), array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('unit_weight', 'Unit Weight') }}
        {{ Form::text('unit_weight', Input::old('unit_weight'), array('class' => 'form-control')) }}
    </div>
    <div class="form-group">
        {{ Form::label('note', 'Notes') }}
        {{ Form::textarea('note', Input::old('note'), array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Create the Product!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

@stop