@extends('admin.layout')
@section('content')

@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>ID</td>
            <td>Name</td>
            <td>Email</td>
            <td>Actions</td>
        </tr>
    </thead>
    <tbody>
    @foreach($category as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->cname }}</td>
            <td>{{ $value->description }}</td>

            <!-- we will also add show, edit, and delete buttons -->
            <td>

                <!-- delete the category (uses the destroy method DESTROY /category/{id} -->
                {{ Form::open(array('url' => 'admin/category/'.$value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete this Category', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}

                <!-- show the category (uses the show method found at GET /category/{id} -->
                <a class="btn btn-small btn-success" href="{{ URL::to('admin/category/' . $value->id) }}">Show this Category</a>

                <!-- edit this category (uses the edit method found at GET /category/{id}/edit -->
                <a class="btn btn-small btn-info" href="{{ URL::to('admin/category/' . $value->id . '/edit') }}">Edit this Category</a>

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<?php echo $category->links(); ?>
@stop