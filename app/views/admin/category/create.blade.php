@extends('admin.layout')
@section('content')

<h1>Create a Category</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::open(array('url' => 'admin/category')) }}

    <div class="form-group">
        {{ Form::label('cname', 'Name') }}
        {{ Form::text('cname', Input::old('cname'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('description', 'Description') }}
        {{ Form::textarea('description', Input::old('description'), array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('parent_id', 'Parent Category') }}
        {{ Form::select('parent_id', $all_categories, array('class' => 'form-control')) }}
    </div>

    {{ Form::submit('Create the Category!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

@stop