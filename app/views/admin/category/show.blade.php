@extends('admin.layout')
@section('content')

<h1>{{ $category->categoryname }}</h1>

    <div class="form-group">
        {{ Form::label('cname', 'Name') }}
        {{ $category->cname }}
    </div>

    <div class="form-group">
        {{ Form::label('description', 'Description') }}
        {{ $category->description }}
    </div>

    <div class="form-group">
        {{ Form::label('parent_id', 'Parent Category') }}
        {{ $category->parent_category }}
    </div>
@stop