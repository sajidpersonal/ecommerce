@extends('admin.layout')
@section('content')

<h1>Edit {{ $category->cname }}</h1>

<!-- if there are creation errors, they will show here -->
{{ HTML::ul($errors->all()) }}

{{ Form::model($category, array('route' => array('admin.category.update', $category->id), 'method' => 'PUT')) }}

    <div class="form-group">
        {{ Form::label('cname', 'Name') }}
        {{ Form::text('cname', $category->cname, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('description', 'Description') }}
        {{ Form::textarea('description', $category->description, array('class' => 'form-control')) }}
    </div>

    <div class="form-group">
        {{ Form::label('parent_id', 'Parent Category') }}
        {{ Form::select('parent_id', $all_categories, $category->parent_id) }}
    </div>

{{ Form::submit('Update the Category!', array('class' => 'btn btn-primary')) }}

{{ Form::close() }}

@stop