@extends('admin.layout')
@section('content')

<h1>Errors Log</h1>

@if (Session::has('message'))
    <div class="alert alert-info">{{ Session::get('message') }}</div>
@endif
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <td>ID</td>
            <td>Error</td>
            <td>Created At</td>
            <td>Actions</td>
        </tr>
    </thead>
    <tbody>
    @foreach($el as $key => $value)
        <tr>
            <td>{{ $value->id }}</td>
            <td>{{ $value->error }}</td>
            <td>{{ $value->created_at }}</td>

            <!-- we will also add show, edit, and delete buttons -->
            <td>

                <!-- delete the category (uses the destroy method DESTROY /category/{id} -->
                {{ Form::open(array('url' => 'admin/errors_log/'.$value->id, 'class' => 'pull-right')) }}
                    {{ Form::hidden('_method', 'DELETE') }}
                    {{ Form::submit('Delete this Error', array('class' => 'btn btn-warning')) }}
                {{ Form::close() }}

            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<?php echo $el->links(); ?>
@stop